var name = "fred"; //string
var male = true; //boolean
var age = 50; //int
var flintstones = ["fred", "wilma", "pebbles"]; //array
var fred = {
    name: name,
    email: "fred@stones.com",
    phone: "12345",
    greet: function () {
        console.log("Hi, my name is %s", this.name);
    }
}; //object
//fred is an instance
var barney = fred; //pointing to the fred instance
barney["name"] = "Barney"; //will override fred

//loop over array
for (i in flintstones) {
    console.log(flintstones[i]);
}

//loop over object
for (i in fred) {
    console.log(i + ": " + fred[i]);
}

// (function() {
//     alert("IIFE")
// })();


//RHS anonymouse function
var world = function (name) {
    console.log("hello %s", name);
}

console.log(world);

world("Bill");

fred.greet();


var h = fred.greet;
console.log(h);
var i = fred.greet();
console.log(i);

h();

//global main object
for (i in this) {
    console.log(i);
}

var makePerson = function (name, email, phone, age) {
    return {
        name: name,
        email: email,
        phone: phone,
        age: age,
        hello: function() {
            return "Hello my name is " + this.name;
        }
    };

};

var pebbles = makePerson("Pebbles", "p@flintstones.com", 12345678, 21);
console.log(pebbles);
pebbles.name = "test";
console.log(pebbles.hello());

//JS object definition
var Person = function(name, email, phone, age) {
    var self = this; //maintain this instance copy
    //instance variables
    self.name = name;
    self.email = email;
    self.phone = phone;
    self.age = age;
}

var mike = new Person("Mike", "m@stones.com", 387438473, 33);
console.log(mike);

//prototypical inheritance
Person.prototype.hello = function() {
    console.log("Hello, my name is " + this.name);
}

console.log(mike);

//Literals
3
"fred"
true
[]
{}
(function() {

})();