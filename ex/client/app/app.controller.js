
(function () {
    "use strict";
    angular.module("MyApp").controller("MyAppCtrl", MyAppCtrl);

    MyAppCtrl.$inject = ["MyAppService"];

    function ImageSet(tag, images) {
        this.tag = tag;
        this.images = images;
    }

    function MyAppCtrl(MyAppService) {
        var self = this; // vm
        self.tag = "";
        self.cart = [];

        self.addToCart = function () {
            if (self.tag == "") {
                return;
            }

            MyAppService.getImages(self.tag).then(function (result) {
                var item = new ImageSet(self.tag, result);
                self.cart.unshift(item);
                self.tag = ""; //reset
            }, function (err) {
                console.log(err);
            });
            // $http.get("https://api.giphy.com/v1/gifs/search", {
            //     params: {
            //         api_key: "ac419443623246189d35e51eab60c1d9",
            //         q: self.tag,
            //         limit: 5,
            //         offset: 0
            //     }
            // }).then(function (result) {
            //     // console.log(result.data.data[0]);
            //     var data = result.data.data;
            //     var images = []
            //     for(var i in data) {
            //         images.push(data[i].images.fixed_height_small.url);
            //     }
            //     var tag = self.tag;
            //     console.log(tag);
            //     var item = new ImageSet(self.tag, images);
            //     self.cart.push(item);
            //     self.tag = ""; //reset
            // });           
        }
    }

})();